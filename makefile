cc=gcc
main=main
cflags=-Wall -Wextra

${main} : ${main}.c
	${cc} ${cflags} -o $@ $<

clean : 
	rm -f ${main}
