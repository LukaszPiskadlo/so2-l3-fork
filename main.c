#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>

int calcHalfLength(int length);

int main(int argc, char* argv[])
{
    if (argc < 2)
    {
        fprintf(stderr, "Brak argumentow programu!\n");
        exit(EXIT_FAILURE);
    }

    char* arg = argv[argc - 1];
    size_t length = strlen(arg);
    int halfLength = calcHalfLength((int)length);

    char* firstHalf = (char*)malloc((halfLength + 1) * sizeof(char*));
    char* secondHalf = (char*)malloc((halfLength + 1) * sizeof(char*));
    strncpy(firstHalf, arg, halfLength);
    firstHalf[halfLength] = '\0';
    strncpy(secondHalf, arg + halfLength, halfLength);
    secondHalf[halfLength] = '\0';

    char** args = (char**)malloc((argc + 2) * sizeof(char*));
    for (int i = 0; i < argc; i++)
    {
        args[i] = (char*)malloc(strlen(argv[i]) + 1);
        strcpy(args[i], argv[i]);
    }
    args[argc + 1] = NULL;

    if (length > 1)
    {
        pid_t childPid = fork();
        if (childPid == 0)
        {
            args[argc] = firstHalf;
            execv(argv[0], args);
        }
        else if (childPid > 0)
        {
            childPid = fork();
            if (childPid == 0)
            {
                args[argc] = secondHalf;
                execv(argv[0], args);
            }
        }

        while (wait(NULL) > 0);
    }

    printf("%d ", getpid());
    for (int i = 1; i < argc; i++)
    {
        printf("%s ", argv[i]);
        free(args[i]);
    }
    printf("\n");
    
    free(args);
    free(firstHalf);
    free(secondHalf);

    return EXIT_SUCCESS;
}

int calcHalfLength(int length)
{
    int powerOf2 = 1;
    while (length >>= 1)
    {
        powerOf2 <<= 1;
    }
    return powerOf2 / 2;
}
